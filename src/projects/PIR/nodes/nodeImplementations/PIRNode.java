package projects.PIR.nodes.nodeImplementations;

import java.awt.Color;
import java.awt.Graphics;
import java.util.LinkedList;

import projects.PIR.nodes.messages.Ack;
import projects.PIR.nodes.messages.Brd;
import projects.defaultProject.nodes.messages.Msg;
import projects.defaultProject.nodes.nodeImplementations.MonoInitiator;
import sinalgo.gui.transformation.PositionTransformation;

public class PIRNode extends MonoInitiator {
	
	public void initialization() {
	}
	
	public void spontaneously() {
	}
	
	public void receipt(LinkedList<Msg> inbox) {
    }

    public void draw(Graphics g, PositionTransformation pt, boolean highlight){
    	this.setColor(Color.green);
    	String text = ""+this.ID;
    	super.drawNodeAsDiskWithText(g, pt, highlight, text, 20, Color.black);
    }	
    
}
