package projects.ChangRoberts2.nodes.nodeImplementations;

import java.awt.Color;
import java.awt.Graphics;
import java.util.LinkedList;

import projects.ChangRoberts2.nodes.messages.Election;
import projects.defaultProject.nodes.messages.Msg;
import projects.defaultProject.nodes.nodeImplementations.MultiInitiator;
import sinalgo.gui.transformation.PositionTransformation;


public class ChangRobertsNode extends MultiInitiator{
	
	// Current leader for this node
	boolean leader, initiator;
	
	// Initialize node as non-participant and set colors
	public void initialization() {
		leader = false;
		initiator = false;
	}
	
	// Set node as initiator and send message to successor
	public void spontaneously() {
		// Initiator code
		initiator = true;
		this.sendSuccessor(new Election(this.ID));
	}
	
	public void receipt(LinkedList<Msg> inbox) {
		
		// Verifying if messages have been received
		if (inbox.size() > 0) {
			
			// Pop the first message (FIFO)
			Election msg = (Election) inbox.pop();
			
			if (initiator) {
				if (msg.getId() < this.ID) sendSuccessor(msg); // Relay the message
				// If ID in message is this node's ID
				else if (msg.getId() == this.ID) {
					leader = true;
					decide();
				}
			}
			else {
				sendSuccessor(msg); // Relay the message
			}	
		}
    }
	
	// Code for graphical visualization
	public void draw(Graphics g, PositionTransformation pt, boolean highlight){
    	this.setColor(nodeColor());
    	
    	String text = ""+this.ID;
    	super.drawNodeAsDiskWithText(g, pt, highlight, text, 20, Color.black);
    }
	
	// Graphical node color depending on state
	public Color nodeColor() {
		if (leader) return Color.red;
		if (initiator) return Color.orange;
		else return Color.gray;
	}
}
