package projects.ChangRoberts2.nodes.messages;

import projects.defaultProject.nodes.messages.Msg;

public class Election extends Msg{

	// Attributs	
	private int id;
	
	// Constructeurs
	public Election(int _id) {
		id = _id;
	}
	
	// Getters - Setters
	public int getId() {
		return id;
	}
}
