package projects.ItaiAndRodeh.nodes.messages;

import projects.defaultProject.nodes.messages.Msg;

public class Message extends Msg{
	
	// Attributs
	private int tag;
	private int count;
	private boolean bool;
	
	// Constructeurs
	public Message(int _tag) {
		tag = _tag;
		count = 1;
		bool = false;
	}
	
	// Getters - Setters
	public int getTag() {
		return tag;
	}
	
	public int getCount() {
		return count;
	}
	
	public boolean getBool() {
		return bool;
	}
	
	public void increaseCount() {
		count++;
	}
	
	public void flipBool() {
		bool = !bool;
	}
}
