package projects.ItaiAndRodeh.nodes.nodeImplementations;

import java.awt.Color;
import java.awt.Graphics;
import java.util.LinkedList;
import java.util.Random;

import projects.ItaiAndRodeh.nodes.messages.Message;
import projects.defaultProject.nodes.messages.Msg;
import projects.defaultProject.nodes.nodeImplementations.MultiInitiator;
import sinalgo.gui.transformation.PositionTransformation;


public class ItaiAndRodehNode extends MultiInitiator{
	
	// Current leader for this node
	boolean leader, isActive;
	int tag;
	Random r = new Random();
	
	// Initialize node as non-participant and set colors
	public void initialization() {
		leader = false;
		isActive = false;
	}
	
	// Choose random tag, set node to active and send message to successor
	public void spontaneously() {
		// Initiator code
		tag = r.nextInt(nbProcesses()) + 1;
		isActive = true;
		this.sendSuccessor(new Message(this.tag)); // Send a message to the next node in the ring
	}
	
	public void receipt(LinkedList<Msg> inbox) {
		
		// Verifying if messages have been received
		if (inbox.size() > 0) {
			
			// Pop the first message (FIFO)
			Message msg = (Message) inbox.pop();
			
			// If node is still active
			if (isActive) {
				
				// If msg tag less than node's tag, set inactive and relay message
				if (msg.getTag() < tag) {
					isActive = false;
					msg.increaseCount();
					sendSuccessor(msg);
				}
				// If tag is node's tag
				else if (msg.getTag() == tag) {
					// Relay message
					if (msg.getCount() < nbProcesses()) {
						msg.increaseCount();
						msg.flipBool();
						sendSuccessor(msg);
					}
					else if (msg.getCount() == nbProcesses()) {
						if (msg.getBool() == false) decide(); // End of algorithm, leader is declared
						else spontaneously(); // Start a new wave
					}
				}
			}
			// If node is a follower
			else {
				msg.increaseCount();
				sendSuccessor(msg); // Relay
			}
		}
    }
	
	// Code for graphical visualization
	public void draw(Graphics g, PositionTransformation pt, boolean highlight){
    	this.setColor(nodeColor());
    	
    	String text = ""+this.ID;
    	super.drawNodeAsDiskWithText(g, pt, highlight, text, 20, Color.black);
    }
	
	// Node color depending of his state
	public Color nodeColor() {
		if (leader) return Color.red;
		if (isActive) return Color.orange;
		else return Color.gray;
	}
}
