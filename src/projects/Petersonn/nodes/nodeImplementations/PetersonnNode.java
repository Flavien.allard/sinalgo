package projects.Petersonn.nodes.nodeImplementations;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.LinkedList;

import projects.Petersonn.nodes.messages.Election;
import projects.defaultProject.nodes.messages.Msg;
import projects.defaultProject.nodes.nodeImplementations.BasicNode;
import sinalgo.gui.transformation.PositionTransformation;


public class PetersonnNode extends BasicNode{
	
	// Current leader for this node
	boolean leader, isPassive, isReady;
	int tag;
	
	// Initialize node as non-participant and set colors
	public void initialization() {
		tag = this.ID;
		isPassive = false;
	}
	
	public void spontaneously() {
		// Initiator code
		this.sendSuccessor(new Election(this.tag)); // Send a message to the next node in the ring
	}
	
	public void receipt(LinkedList<Msg> inbox) {
		
		// Verifying if messages have been received
		if (inbox.size() > 0) {
			
			// Pop the first message (FIFO)
			Election msg = (Election) inbox.pop();
			
			// If node is passive, relay message
			if (isPassive) {
				sendSuccessor(msg);
				return;
			}
			
			// If list contains 2 IDS in its list
			if (msg.getIDS().size() >= 2) {
				// If predecessor tag is not the lower, set passive
				if (getMinimumTag(msg.getIDS()) != msg.getIDS().get(1))
					isPassive = true;
				// Get predecessor tag and restart
				else {
					tag = msg.getIDS().get(1);
					sendSuccessor(new Election(this.tag));
				}
			}
			// If list has 1 ID in it
			else if (msg.getIDS().size() == 1){
				// If the only tag is this node's ID, set leader
				if(msg.getIDS().get(0) == this.tag) {
					leader = true;
					decide();
				}
				// Relay if not
				else {
					msg.addId(tag);
					sendSuccessor(msg);
				}
			}
		}
    }
	
	// Get the lower tag of the list
	private int getMinimumTag(ArrayList<Integer> list) {
		int min = (list.get(0) < list.get(1) ? list.get(0) : list.get(1));
		return (min < this.tag ? min : this.tag);
	}
	
	// Code for graphical visualization
	public void draw(Graphics g, PositionTransformation pt, boolean highlight){
    	this.setColor(nodeColor());	
    	// String text = ""+this.ID+":"+this.tag;
    	String text = ""+this.tag;
    	super.drawNodeAsDiskWithText(g, pt, highlight, text, 20, Color.black);
    }
	
	// Set graphical color for node depending of its state
	public Color nodeColor() {
		if (leader) return Color.red;
		if (isPassive) return Color.magenta;
		else return Color.green;
	}
}
