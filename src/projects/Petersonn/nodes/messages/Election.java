package projects.Petersonn.nodes.messages;

import java.util.ArrayList;
import java.util.List;

import projects.defaultProject.nodes.messages.Msg;

public class Election extends Msg{
	
	// Attributs
	// Max list size
	private static int MAX_SIZE = 2;
	
	private ArrayList<Integer> ids = new ArrayList<Integer>(MAX_SIZE);
	
	// Constructeurs
	public Election(int _id) {
		ids.add(_id);
	}
	
	// Getters - Setters
	public void addId(int _id) {
		ids.add(_id);
	}
	
	public boolean isFull() {
		return (ids.size() == MAX_SIZE) ?  true: false;
	}
	
	public ArrayList<Integer> getIDS(){
		return ids;
	}
}
