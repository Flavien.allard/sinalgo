package projects.dfs.nodes.nodeImplementations;

import java.awt.Color;
import java.awt.Graphics;
import java.util.LinkedList;

import projects.defaultProject.nodes.messages.Msg;
import projects.defaultProject.nodes.nodeImplementations.MonoInitiator;
import projects.dfs.nodes.messages.Jeton;
import sinalgo.gui.transformation.PositionTransformation;

public class DfsNode extends MonoInitiator {
    public int pere;
    public boolean visite[];
    public Color couleur=Color.blue;	
	
	public void initialization() {
		this.visite=new boolean[this.degree()+1];
		
		for(int i=0;i<=this.degree();i++) this.visite[i]=false;
		
		this.pere=0; // 0 correspond à NIL dans l'algorithme	 
	}
	
	public void spontaneously() {
	    this.visite[1]=true;
	    this.pere=-1;  // -1 correspond à la valeur TOP dans l'algorithme
	    this.send(new Jeton(),1);
	    this.inverse();	
	} 

    // La méthode fini() detecte la terminaison locale du parcours
    boolean fini(){
    	int i= 1;
    	while(i<=this.degree()){
    		if(!this.visite[i]) return false;
    		i++;
    	}
    	return true;
    }
    
    // La méthode nextVisite() retourne le prochain canal à visiter
    int nextVisite(){
    	for(int i=1;i<=this.degree();i++) {
    		if(this.pere !=i && !this.visite[i])
    		{
    			return i;
    		}
    	}
    	return this.pere;
    }
    
    // Vous utiliserez la méthode ci-dessous pour changer la couleur
    // d'un noeud lors de la première visite de ce noeud
    public void inverse(){
    	if(this.couleur==Color.blue)
    		this.couleur=Color.red;
    	else 
    		this.couleur=Color.blue;
    }
    
	public void receipt(LinkedList<Msg> inbox) {
		for(Msg m: inbox) {
			if(m instanceof Jeton) 
	    	{	
				/* Partie à compléter */
	    	}
		}
	}
    
    public void draw(Graphics g, PositionTransformation pt, boolean highlight){
    	this.setColor(this.couleur);
    	String text = ""+this.ID;
    	super.drawNodeAsDiskWithText(g, pt, highlight, text, 20, Color.black);
    }	   
}
