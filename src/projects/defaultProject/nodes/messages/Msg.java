package projects.defaultProject.nodes.messages;

import sinalgo.nodes.Node;
import sinalgo.nodes.messages.Message;

// Les messages utilisés dans les algorithmes doivent être des classes dérivées de la classe Msg.
// La référence sender est cachée à l'utilisateur.
// Elle permet de retrouver le numéro de canal entrant, cf. méthode From(m) dans BasicNode.

public class Msg extends Message {
	
	public Node sender=null; 
	
	public Msg(Node s){
		this.sender=s;
	}
	
	public Msg() {
		this(null);
	}
	
	public Msg clone(){
		return new Msg(this.sender);
	}
}
