package projects.defaultProject.nodes.messages;

import sinalgo.nodes.Node;
import sinalgo.nodes.messages.Message;

// La classe Container est utilisée en interne pour stocker la référence de l'émetteur d'un message.

public class Container extends Message {
	
	public Node sender;
	public Msg m;
	
	public Container(Node s, Msg m){
		this.sender=s;
		this.m=m;
	}	
	
	public Message clone(){
		return new Container(this.sender,this.m);
	}
}
