package projects.LE.nodes.messages;


import projects.defaultProject.nodes.messages.Msg;

public class Ack extends Msg {
public int id;

public Ack(int id){
	this.id=id;
}

public Msg clone() {
	return new Ack(id);
}

	
}
