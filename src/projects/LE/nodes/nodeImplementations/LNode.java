package projects.LE.nodes.nodeImplementations;

import java.awt.Color;
import java.awt.Graphics;
import java.util.LinkedList;

import projects.LE.nodes.messages.Ack;
import projects.LE.nodes.messages.Brd;
import projects.defaultProject.nodes.messages.Msg;

import projects.defaultProject.nodes.nodeImplementations.MultiInitiator;

import sinalgo.gui.transformation.PositionTransformation;

public class LNode extends MultiInitiator{
	
  
	public void initialization() {
	}
	
	public void spontaneously() {
	}
    
	public void receipt(LinkedList<Msg> inbox) {
    }
    
    public void draw(Graphics g, PositionTransformation pt, boolean highlight){
    	this.setColor(Color.green);
    	String text = "";
    	super.drawNodeAsDiskWithText(g, pt, highlight, text, 20, Color.black);
    }	
    
}
