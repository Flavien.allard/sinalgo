package projects.TokenTree.nodes.nodeImplementations;

import java.awt.Color;
import java.awt.Graphics;
import java.util.LinkedList;

import projects.TokenTree.nodes.messages.Jeton;
import projects.defaultProject.nodes.messages.Msg;
import projects.defaultProject.nodes.nodeImplementations.MonoInitiator;
import sinalgo.gui.transformation.PositionTransformation;

public class TokenTreeNode extends MonoInitiator{

	// variables utiles pour l'affichage uniquement
	public boolean jeton;
	public int parent;
	
	public void initialization() {
		jeton=false; // pour l'affiche des processus, on distingue si le processus a déjà reçu le jeton
		parent=0; // pour l'affichage des arêtes
	}
	
	public void spontaneously() {
		this.send(new Jeton(),1);
		this.jeton=true;
	}
	
	public void receipt(LinkedList<Msg> inbox) {
	for(Msg m: inbox) {
		if(m instanceof Jeton) // Inutile dans ce cas, mais utile lorsqu'il y a plusieurs types de message
			{	
			Jeton msg = (Jeton) m;
			if(this.initiator() && this.from(msg)==this.degree()) this.decide();
			else
				{
				this.jeton=true;
				if (this.parent==0 && !this.initiator()) this.parent=this.from(msg);
				this.send(new Jeton(),(this.from(msg)%this.degree())+1);
				}
			}
		}
	}
	
	
	public Color Couleur(){
		if(this.jeton) return Color.red;
		return Color.blue;
	}
	
	// Affichage du processus :
	// en rouge, puis bleu après qu'il est reçu le jeton pour la première fois.
	// On affiche aussi l'identité du noeud.
	// Rappel : l'identité de l'initiateur est 1.
	
	 public void draw(Graphics g, PositionTransformation pt, boolean highlight){
	 this.setColor(this.Couleur());
	 String text = ""+this.ID;
	 super.drawNodeAsDiskWithText(g, pt, highlight, text, 20, Color.black);
	 }
	
	
}
