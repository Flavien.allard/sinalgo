package projects.TokenTree.nodes.edges;



import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import projects.TokenTree.nodes.nodeImplementations.TokenTreeNode;
import sinalgo.gui.helper.Arrow;
import sinalgo.gui.transformation.PositionTransformation;
import sinalgo.nodes.Position;
import sinalgo.nodes.edges.BidirectionalEdge;


// Sur-définition d'une arête pour faire un affichage dédié plus compréhensible

public class Arete extends BidirectionalEdge {
	
	// La méthode ci-dessous dessine une arête
	
	public void draw(Graphics g, PositionTransformation pt) {
		
		// p1 donne les coordonnées d'un processus incident à l'arête
		Position p1 = startNode.getPosition();
		
		// On se translate en p1
		pt.translateToGUIPosition(p1);
		
		// Sauvegarde des coordonnées de p1
		int fromX = pt.guiX, fromY = pt.guiY;
		
		// p2 donne les coordonnées du deuxième processus incident à l'arête
		Position p2 = endNode.getPosition();
		
		// On se translate en p2
		pt.translateToGUIPosition(p2);
		
		// On caste en TokenTreeNode pour pouvoir accéder aux variables publiques des deux processus incidents
		TokenTreeNode deb=(TokenTreeNode) this.startNode;
		TokenTreeNode fin=(TokenTreeNode) this.endNode;
		
		// Par défaut l'arête sera dessinée en rouge (si elle contient des messages en fait)
		Color c=Color.red;
		
		// g2 est une boite à outils graphique 
		Graphics2D g2=(Graphics2D) g;
		
		// Si le processus deb désigne le processus fin comme parent (ATTENTION l'arête est orientée)
		if(deb.parent !=0 && (deb.getNeighbor(deb.parent)) == fin){	
		   
			// si l'arête ne contient pas de message dans les deux sens, alors on la colore en bleu.
		    if(this.numberOfMessagesOnThisEdge==0 && this.oppositeEdge.numberOfMessagesOnThisEdge==0)
				c=Color.blue;
		    
		    // On grossit le trait
		    Stroke stroke = new BasicStroke(5f, BasicStroke.CAP_ROUND,BasicStroke.JOIN_MITER, 10.0f); 
		    g2.setStroke(stroke);
		    
		    // Et on affiche une flèche de deb vers fin.
		    Arrow.drawArrow(fromX, fromY, pt.guiX, pt.guiY, g2, pt,c);		
		   }
		else {
			
			// Sinon, on dessine un trait plus fin de deb vers fin uniquement dans le cas où fin ne désigne pas deb comme parent.
			// Si on dessine ce trait et que l'arête ne contient pas de message dans les deux sens, alors l'arête est de couleur noire.
		
			Stroke stroke = new BasicStroke(2f, BasicStroke.CAP_ROUND,BasicStroke.JOIN_MITER, 10.0f); 
			g2.setStroke(stroke);
			
			if(fin.parent != fin.getIndex(deb)) {
				if(this.numberOfMessagesOnThisEdge==0 && this.oppositeEdge.numberOfMessagesOnThisEdge==0)
					c=Color.black;
				g2.setColor(c);
				g.drawLine(fromX, fromY, pt.guiX, pt.guiY);				
				}
		  }
	}
	
 
}
