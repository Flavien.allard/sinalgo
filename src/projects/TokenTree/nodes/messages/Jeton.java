package projects.TokenTree.nodes.messages;


import projects.defaultProject.nodes.messages.Msg;

// Dans la circulation de jeton, nous avons un seul type de message.
// Il dérive de la classe Msg.

public class Jeton extends Msg {

	public Jeton(){
	}	

	public Msg clone() {
		return new Jeton();
	}
	
}
