package projects.LeLann.nodes.nodeImplementations;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import projects.ChangRoberts2.nodes.messages.Election;
import projects.defaultProject.nodes.messages.Msg;
import projects.defaultProject.nodes.nodeImplementations.MultiInitiator;
import sinalgo.gui.transformation.PositionTransformation;


public class LeLannNode extends MultiInitiator{
	
	// Current leader for this node
	boolean initiator, leader;
	
	List<Integer> initiateurs = new ArrayList<Integer>();
	
	// Initialize node as non-participant
	public void initialization() {
		initiator = false;
	}
	
	// Init list with initiators ID ans send Election message to successor
	public void spontaneously() {
		// Initiator code
		initiator = true;
		initiateurs.add(this.ID);
		this.sendSuccessor(new Election(this.ID)); // Send a message to the next node in the ring
	}
	
	public void receipt(LinkedList<Msg> inbox) {
		
		// Verifying if messages have been received
		if (inbox.size() > 0) {
			
			// Pop the first message (FIFO)
			Election msg = (Election) inbox.pop();
			
			// If msg contains this node's ID, find leader process
			if (msg.getId() == this.ID)
				if (getChosenProcess() == this.ID) {
					leader = true;
					decide();
				}	
			// Add ID to msg if before relaying
			if (initiator) {
				initiateurs.add(msg.getId());
			}
			sendSuccessor(msg);
		}
    }
	
	// Code for graphical visualization
	public void draw(Graphics g, PositionTransformation pt, boolean highlight){
    	this.setColor(nodeColor());	
    	String text = ""+this.ID;
    	super.drawNodeAsDiskWithText(g, pt, highlight, text, 20, Color.black);
    }
	
	// Find lower ID process to get chosen as leader
	public int getChosenProcess() {
		
		int min = this.ID;
		for (int i : initiateurs)
			if (i < min) min = i;
		return min;
	}
	
	// Color of node depending on state
	public Color nodeColor() {
		
		if (leader) return Color.red;
		if (initiator) return Color.orange;
		else return Color.gray;
	}
}
